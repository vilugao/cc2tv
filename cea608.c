#include "cea608.h"

unsigned char cea608_channel1(unsigned char firstbyte)
{
	if ((firstbyte & 0x70) == 0x10)
		firstbyte &= ~0x08;
	return firstbyte;
}

unsigned char cea608_channel2(unsigned char firstbyte)
{
	if ((firstbyte & 0x70) == 0x10)
		firstbyte |= 0x08;
	return firstbyte;
}

unsigned char cea608_field1(const unsigned char bytes[2])
{
	if ((bytes[0] & 0x77) == 0x15 && (bytes[1] & 0x70) == 0x20)
		return bytes[0] & ~1;
	return bytes[0];
}

unsigned char cea608_field2(const unsigned char bytes[2])
{
	if ((bytes[0] & 0x77) == 0x14 && (bytes[1] & 0x70) == 0x20)
		return bytes[0] | 1;
	return bytes[0];
}

unsigned char cea608_add_parity(unsigned char byte)
{
	unsigned char parity = byte & 0x7f;
	parity ^= (parity << 4) | (parity >> 4);
	parity ^= parity << 2;
	parity ^= parity << 1;
	return (~parity & 0x80) | (byte & 0x7f);
}

#if 0
unsigned char * cea608_escapes_to_cc1(char c)
{
	switch (c) {
	case '\a':
		return CC1_FON;
	case '\b':
		return CC1_BS;
	case '\t':
		return "    ";
	case '\f':
		return CC1_TR;
	case '\n':
	case '\r':
		return CC1_CR;
	default:
		break;
	}
	return NULL;
}
#endif
