#include <avr/io.h>
#include <avr/power.h>

#ifndef USART_MAX_BUFFSZ
#define USART_MAX_BUFFSZ (1 << 6)
#endif

#ifndef BAUD
	#if 0
		/* baud rate = 600 */
		#define BAUD (29.97 * 2 * (8 + 2))
	#else
		#define BAUD 9600
	#endif
#endif
#include <util/setbaud.h>

#include "usart_buffer.h"

#ifndef ENABLED_XONXOFF
/* TODO: Software flow control not working? */
#define ENABLED_XONXOFF 0
#endif

#if 0
/* Begin of defines for ATmega8 */
#ifndef UBRR0
#define UBRR0 UBRR
#endif

#ifndef UCSR0A
#define UCSR0A UCSRA
#endif

#ifndef UCSR0B
#define UCSR0B UCSRB
#endif

#ifndef UCSR0C
#define UCSR0C UCSRC
#endif

#ifndef UDR0
#define UDR0 UDR
#endif

#ifndef DOR0
#define DOR0 DOR
#endif

#ifndef FE0
#define FE0 FE
#endif

#ifndef RXC0
#define RXC0 RXC
#endif

#ifndef RXEN0
#define RXEN0 RXEN
#endif

#ifndef UCSZ01
#define UCSZ01 UCSZ1
#endif

#ifndef UCSZ00
#define UCSZ00 UCSZ0
#endif
/* End of defines for ATmega8 */
#endif

static struct {
	char data[USART_MAX_BUFFSZ];
	char *p_write;
	char *p_read;
} usart_buffer;

#if USART_MAX_BUFFSZ >= 256
#define CURR_USART_BUFFLEN \
	((USART_MAX_BUFFSZ \
		+ usart_buffer.p_write \
		- usart_buffer.p_read) \
	% USART_MAX_BUFFSZ)
#else
#define CURR_USART_BUFFLEN \
	((unsigned char)(USART_MAX_BUFFSZ \
		+ usart_buffer.p_write \
		- usart_buffer.p_read) \
	% USART_MAX_BUFFSZ)
#endif

#define USART_BUFFEND (usart_buffer.data + USART_MAX_BUFFSZ - 1)

#if ENABLED_XONXOFF
#define CHAR_XON  0x11
#define CHAR_XOFF 0x13
#endif

void usart_buffer_clear(void)
{
	usart_buffer.p_read = usart_buffer.p_write;
}

static char usart_buffer_read_byte(void)
{
	char c = *usart_buffer.p_read & 0x7F;
	if (++usart_buffer.p_read >= USART_BUFFEND + 1)
		usart_buffer.p_read -= USART_MAX_BUFFSZ;
	return c;
}

unsigned char usart_buffer_read_bytes(
		unsigned char *data,
		unsigned char len)
{
	unsigned char i = 0;

	if (CURR_USART_BUFFLEN < len)
		return 0;

	do {
		*data++ = usart_buffer_read_byte();
		i++;
	} while (--len);

#if ENABLED_XONXOFF
	while (!(UCSR0A & (1<<UDRE0)))
		;
	UDR0 = CHAR_XON;
#endif

	return i;
}

enum usart_buffer_status usart_buffer_recv_byte_ifavail(void)
{
	unsigned char ucsr;

	if (CURR_USART_BUFFLEN >= USART_MAX_BUFFSZ - 1)
		return DATA_OVERRUN;

	ucsr = UCSR0A;
	if (!(ucsr & (1<<RXC0)))
		return NO_DATA;

	*usart_buffer.p_write = UDR0;

	if (ucsr & (1<<DOR0))
		return DATA_OVERRUN;
	if (ucsr & (1<<FE0))
		return FRAME_ERROR;

	if (++usart_buffer.p_write >= USART_BUFFEND + 1)
		usart_buffer.p_write -= USART_MAX_BUFFSZ;

#if ENABLED_XONXOFF
	if (CURR_USART_BUFFLEN >= USART_MAX_BUFFSZ / 2) {
		while (!(UCSR0A & (1<<UDRE0)))
			;
		UDR0 = CHAR_XOFF;
	}
#endif

	return OK;
}

void usart_buffer_init(void)
{
	usart_buffer.p_write = usart_buffer.p_read = usart_buffer.data;

	power_usart0_enable();

	UBRR0 = UBRR_VALUE;
	//UCSR0C = (1<<UCSZ01) | (1<<UPM01) | (1<<UPM00); /* 7,O,1 */
	UCSR0C = (1<<UCSZ01) | (1<<UCSZ00); /* 8,N,1 */
#if ENABLED_XONXOFF
	/* TXEN for XON/XOFF Flow Control */
	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
#else
	UCSR0B = (1<<RXEN0);
#endif
}
