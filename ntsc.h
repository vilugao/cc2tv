#ifndef NTSC_H
#define NTSC_H

#define NTSC_FRAME_LINES  525
#define NTSC_FIELD_LINES  ((NTSC_FRAME_LINES - 1) / 2)
#define NTSC_VIDEO_LINES  240
#define NTSC_VBI_LINES  (NTSC_FIELD_LINES - NTSC_VIDEO_LINES)

#define NTSC_TIME_HSYNC  4.7
#define NTSC_TIME_DISPLAYSTART  (2 * NTSC_TIME_HSYNC)

#define NTSC_CYCLES_SCANLINE  (F_CPU / (29.97 * NTSC_FRAME_LINES))
#define NTSC_CYCLES_HSYNC  (NTSC_TIME_HSYNC * F_CPU / 1000000)
#define NTSC_CYCLES_DISPLAYSTART \
	(NTSC_TIME_DISPLAYSTART * F_CPU / 1000000)

#ifndef NTSC_PROGRESSIVE
#define NTSC_PROGRESSIVE 1
#endif

#if NTSC_PROGRESSIVE
/* 0=Active video, 1=Field */
#define ntsc_scanarea GPIOR1
/* VBI=1-21, Active video=0-239 */
#define ntsc_scanline GPIOR0

#define NTSC_EVEN_FIELD 1
#define NTSC_ACTIVE_VIDEO 0
#else
extern unsigned short ntsc_scanline;
#endif

void ntsc_init(void);

#endif
