#ifndef USART_BUFFER_H
#define USART_BUFFER_H

enum usart_buffer_status {
	OK, NO_DATA,
	FRAME_ERROR, DATA_OVERRUN,
};

void usart_buffer_clear(void);
unsigned char usart_buffer_read_bytes(unsigned char *data,
	unsigned char len);
enum usart_buffer_status usart_buffer_recv_byte_ifavail(void);
void usart_buffer_init(void);

#endif
