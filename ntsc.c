#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>

#include "ntsc.h"
#include "video_pins.h"

#if NTSC_PROGRESSIVE
#define NTSC_FANCY_VSYNC 0
#elif !defined(NTSC_FANCY_VSYNC)
#define NTSC_FANCY_VSYNC 0
#endif

#if 0
/* Begin of defines for ATmega8 */
#ifndef TIFR1
#define TIFR1 TIFR
#endif

#ifndef TIMSK1
#define TIMSK1 TIMSK
#endif
/* End of defines for ATmega8 */
#endif

#if !NTSC_PROGRESSIVE
unsigned short ntsc_scanline;
#endif

static void ntsc_vsync_line(void);

static void ntsc_vsync_line(void)
{
	switch (ntsc_scanline) {
#if NTSC_FANCY_VSYNC
	/* In VSYNC (1 to 9 scalines), the PWM frequency is doubled */
	case NTSC_FRAME_LINES + 1:
		current_scanline = 1;
		goto case 1;
	case NTSC_FIELD_LINES + 1: /* 263 */
		ICR1 = NTSC_CYCLES_SCANLINE / 2 - 0.5;
		break;
	case NTSC_FIELD_LINES + 1 *2+0: /* 263 */
		OCR1A = 0.04 * NTSC_CYCLES_SCANLINE - 0.5;
	case 1:
		ICR1 = NTSC_CYCLES_SCANLINE / 2 - 0.5;
		OCR1A = 0.04 * NTSC_CYCLES_SCANLINE - 0.5;
		break;
	case 7 *2-1:
	case NTSC_FIELD_LINES + (7 *2-0): /* 269,5 ~ 276 */
		OCR1A = 0.04 * NTSC_CYCLES_SCANLINE - 0.5;
		break;
	case 4 *2-1:
	case NTSC_FIELD_LINES + (4 *2-0): /* 266,5 ~ 270 */
		OCR1A = 0.07 * NTSC_CYCLES_SCANLINE - 0.5;
		break;
	case 10*2-1:
		ICR1 = NTSC_CYCLES_SCANLINE - 0.5;
		OCR1A = NTSC_CYCLES_HSYNC - 0.5;
		current_scanline = 10;
		next_line = blank_line;
		break;
	case NTSC_FIELD_LINES + (9 *2-1):  /* 272 ~ 279 */
		while (PINB & (1<<VIDEO_SYNC_PIN))
			;
		ICR1 = NTSC_CYCLES_SCANLINE - 0.5;
		OCR1A = NTSC_CYCLES_HSYNC - 0.5;
		current_scanline = NTSC_FIELD_LINES + 10;
		next_line = blank_line;
		break;
#else
#if !NTSC_PROGRESSIVE
	case NTSC_FRAME_LINES + 1:
		ntsc_scanline = 1;
		break;
#endif
	case 3:
#if !NTSC_PROGRESSIVE
	case NTSC_FIELD_LINES + 3:
#endif
		OCR1A = NTSC_CYCLES_SCANLINE - NTSC_CYCLES_HSYNC - 1;
		break;
	case 6:
#if !NTSC_PROGRESSIVE
	case NTSC_FIELD_LINES + 6:
#endif
		OCR1A = NTSC_CYCLES_HSYNC - 1;
		break;
#if NTSC_PROGRESSIVE
	case NTSC_VBI_LINES + 1:
		ntsc_scanarea |= (1<<NTSC_ACTIVE_VIDEO);
		ntsc_scanline = 1;
		break;
#endif
#endif
	}
}

void ntsc_init(void)
{
	PORTB |= (1<<VIDEO_SYNC_PIN);
	DDRB |= (1<<VIDEO_SYNC_PIN);

	PORTB &= ~(1<<VIDEO_PIXEL_PIN);
	DDRB |= (1<<VIDEO_PIXEL_PIN);

	power_timer1_enable();

	/* 16-bit timer for HSYNC NTSC. */
	ICR1 = NTSC_CYCLES_SCANLINE - 1;
	OCR1A = NTSC_CYCLES_HSYNC - 1;
	OCR1B = NTSC_CYCLES_DISPLAYSTART - 1;

	TCCR1A = (3 << COM1A0)		/* Set OC1A on compare match */
		| (0 << COM1B0)		/* OC1B disconnected */
		| ((14 & 3) << WGM10);	/* Fast PWM, */
	TCCR1B = ((14 >> 2) << WGM12)	/* TOP=ICR1  */
		| (1 << CS10);		/* no prescaling */

	TIMSK1 = (1<<TOIE1);
}

ISR(TIMER1_OVF_vect)
{
	TIFR1 |= (1<<OCF1B);
	ntsc_scanline++;
#if NTSC_PROGRESSIVE
	if (ntsc_scanarea & (1<<NTSC_ACTIVE_VIDEO)) {
		if (ntsc_scanline > NTSC_VIDEO_LINES) {
			ntsc_scanarea ^=
				(1<<NTSC_ACTIVE_VIDEO) | (1<<NTSC_EVEN_FIELD);
			ntsc_scanline = 1;
		}
	} else
#endif
		ntsc_vsync_line();
}
