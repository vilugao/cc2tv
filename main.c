#include <avr/cpufunc.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/power.h>
#include <avr/sleep.h>

#ifndef ENABLED_USART
#define ENABLED_USART 1
#endif

#ifndef ENABLED_VIDEO_TEST
#define ENABLED_VIDEO_TEST 1
#endif

#ifndef ENABLED_WATCHDOG
/* Watchdog not enabled by default (because of faulty bootloaders?) */
#define ENABLED_WATCHDOG 0
#endif

#if ENABLED_WATCHDOG
#include <avr/wdt.h>
#endif

#if ENABLED_USART
#include "usart_buffer.h"
#endif

#include "video_pins.h"
#include "ntsc.h"
#include "cea608.h"
#include "line21_gen.h"

#define LED_BUILTIN_PIN PB5

#if 0
/* Begin of defines for ATmega8 */
#ifndef EIFR
#define EIFR GIFR
#endif

#ifndef EICRA
#define EICRA MCUCR
#endif

#ifndef TIFR1
#define TIFR1 TIFR
#endif
/* End of defines for ATmega8 */
#endif

#if ENABLED_VIDEO_TEST
#define VTE_BIT 4
#define video_test_is_enabled() (GPIOR1 & (1<<VTE_BIT))
#define video_test_enable() (GPIOR1 |= (1<<VTE_BIT))
#define video_test_disable() (GPIOR1 &= ~(1<<VTE_BIT))
#endif

#define CC1_POPUP(s) CC1_RCL CC1_ENM s CC1_EDM CC1_EOC
static const char PROGMEM cc_f1_demo_data[] =
	CC1_EDM
	CC1_POPUP(CC1_1512 "\x91\x37" "MUSIC\x80" "\x91\x37")
	"\1"
	"\1"
	CC1_EDM
	CC1_POPUP(CC1_1400 "I AM AT THE LEFT"
	          CC1_1500 "OF THE SCREEN.")
	"\1"
	CC1_POPUP(CC1_1400 "SO CAPTIONS\x80"
	          CC1_1500 "OF WHAT I SAY\x80")
	"\1"
	CC1_POPUP(CC1_1400 "APPEAR AT THE LEFT"
	          CC1_1500 "OF THE SCREEN, TOO.\x80")
	"\1" /* 10 s */
	CC1_EDM
	"\1"
	CC1_POPUP(CC1_1416 CC1_TO2 "NOW I'M AT THE"
	          CC1_1512         "RIGHT OF THE SCREEN,")
	"\1"
	CC1_POPUP(CC1_1416 CC1_TO2 "SO MY CAPTIONS"
	          CC1_1512         "APPEAR AT THE RIGHT.")
	"\1"
	"\1"
	CC1_EDM
	CC1_POPUP(CC1_1504 CC1_TO2 CC1_MRCI "NOW I AM OFF SCREEN.")
	"\1" /* 20 s */
	CC1_POPUP(CC1_1408 CC1_TO2 CC1_MRCI "TO INDICATE\x80"
	          CC1_1504 CC1_TO2 CC1_MRCI "THAT I'M OFF SCREEN,")
	"\1"
	CC1_POPUP(CC1_1408 CC1_TO1 CC1_MRCI "WHATEVER I SAY"
	          CC1_1508 CC1_TO1 CC1_MRCI "IS ITALICIZED.")
	"\1"
	CC1_EDM
	CC1_POPUP(CC1_0104 CC1_TO1 "NOW MY NAME APPEARS AT"
	          CC1_0200 CC1_TO3 "THE BOTTOM OF THE SCREEN,\x80")
	"\1"
	CC1_POPUP(CC1_0104 CC1_TO3 "WE PUT CAPTIONS OF"
	          CC1_0204 CC1_TO1 "WHAT I SAY AT THE TOP,")
	"\1"
	CC1_POPUP(CC1_0104 CC1_TO3 "SO THAT MY NAME IS"
	          CC1_0204         "NOT COVERED BY CAPTIONS.")
	"\1" /* 30 s */
	"\1"
	CC1_POPUP(CC1_1508 CC1_TO1 "UP UNTIL NOW,\x80")
	"\1"
	CC1_POPUP(CC1_1404 CC1_TO3 "WE HAVE BEEN USING"
	          CC1_1508         "POP-ON CAPTIONS.")
	"\1"
	CC1_POPUP(CC1_1408 CC1_TO3 "WHEN A NEW"
	          CC1_1508         "CAPTION POPS ON,")
	"\1"
	CC1_POPUP(CC1_1408         "THE OLD CAPTION\x80"
	          CC1_1508 CC1_TO2 "DISAPPEARS.\x80")
	"\1" /* 40 s */
	CC1_EDM
	"\1"
	CC1_EDM
	CC1_RDC   CC1_1408 CC1_TO3 "THIS IS A\x80"
	          CC1_1504 CC1_TO3 "PAINT-ON CAPTION.\x80"
	"\1"
	CC1_EDM
	CC1_RDC   CC1_1404 CC1_TO3 "ONE CAPTION BLOCK IS"
	          CC1_1504 CC1_TO1 "PAINTED ON THE SCREEN,"
	"\1"
	CC1_EDM
	CC1_RDC   CC1_1404 CC1_TO2 "FROM LEFT TO RIGHT,\x80"
	          CC1_1504         "ONE CHARACTER AT A TIME."
	"\1"
	"\1" /* 50 s */
	CC1_EDM
	CC1_RU3   CC1_CR  CC1_1500  "NOW THE ROLL-UP MODE.\x80"
	"\1"
	CC1_RU3   CC1_CR  CC1_1500  "THIS MODE IS NORMALLY\x80"
	          CC1_CR            "USED FOR LIVE TV PROGRAMS."
	"\1"
	CC1_RU3   CC1_CR  CC1_1500  "CAPTION LINES ROLL UP\x80"
	          CC1_CR            "ONE LINE AT A TIME.\x80"
	"\1"
	"\1"
	CC1_EDM
	CC1_POPUP(CC1_1404 CC1_TO2 "Captions can appear\x80"
	          CC1_1508 CC1_TO1 "in lower case,")
	"\1" /* 60 s */
	CC1_POPUP(CC1_1504 CC1_TO2 "and also in Spanish:")
	"\1"
	/* - ¿Dónde está el niño? - Está en la casa. */
	CC1_POPUP(CC1_1300
	          "\x11\x33" "D\x5Fnde est\x2A el ni\x7Eo?\x80"
	          CC1_1516
	          "Est\x2A en la casa.")
	"\1"
	CC1_POPUP(CC1_1508 CC1_TO1 "And in French:") "\1"
	/* - Parlez-vous Français? - Répétez, s'il vous plaît. */
	CC1_POPUP(CC1_1300
	          "Parlez-vous Fran\x7B""ais?\x80"
	          CC1_1504 CC1_TO3
	          "R\x5Cp\x5Ctez, s'il vous pla\x11\x3Dt.")
	"\1"
	CC1_POPUP(CC1_0104 CC1_TO1 "THE FOLLOWING SPECIAL\x80"
	          CC1_0200 CC1_TO1 "CHARACTERS ARE ALSO AVAILABLE."
	          /* á é í ó ú à è ì ò ù */
	          /* ä ë ï ö ü â ê î ô û */
	          CC1_RCL
	          CC1_1404 CC1_TO2
	          "\x2A \x5C \x5E \x5F \x60 "
	          "\x11\x38 \x80\x11\x3A ?\x13\x24 ?\x13\x26 ?\x12\x3C"
	          CC1_1504 CC1_TO2
	          "?\x80\x13\x31 ?\x12\x36 ?\x12\x39"
	          " ?\x13\x33 ?\x12\x25 \x80"
	          "\x11\x3B \x80\x11\x3C \x80\x11\x3D"
	          " \x80\x11\x3E \x80\x11\x3F"
	) "\1"
	"\1" /* 70 */
	CC1_POPUP(CC1_0100 CC1_TO1 "THEY INCLUDE THE MUSIC SYMBOL\x80"
	          CC1_0204         "AND ACCENTED CHARACTERS."
	          /* ç ã ñ õ å ¿ ¡ ¢ */
	          /* £ ¥ © ® ™ ° ß ♪ */
	          CC1_RCL
	          CC1_1408
	          "\x7B ?\x80\x13\x21 \x7E ?\x13\x28 ?"
	          "\x13\x39 \x80\x11\x33 ?\x12\x27 \x80\x11\x35"
	          CC1_1508
	          "\x11\x36 ?\x13\x35 ?\x12\x2B \x80\x11\x30 \x80"
	          "\x11\x34 \x80\x11\x31 ?\x13\x34 \x80\x11\x37"
	) "\1"
	"\1"
	CC1_EDM
	;

static const char PROGMEM cc_f1_fe_data[] =
	CC1_POPUP(CC1_01R CC1_BBO CC1_FON "[Frame Error!]");
static const char PROGMEM cc_f1_dor_data[] =
	CC1_POPUP(CC1_01R CC1_BBO CC1_FON "[Data OverRun!]\x80");
#undef CC1_POPUP

enum cc_states_t {
	CC_STATE_END, CC_STATE_NEXT, CC_STATE_WAIT,
	CC_STATE_FE, CC_STATE_FE_NEXT,
	CC_STATE_DOR, CC_STATE_DOR_NEXT
};
static enum cc_states_t cc_demo_state = CC_STATE_END;


static void cc_demo_button(void)
{
	if (EIFR & (1<<INTF0)) {
		switch (cc_demo_state) {
		case CC_STATE_WAIT:
			cc_demo_state = CC_STATE_END;
			goto flag_off;
		case CC_STATE_END:
			cc_demo_state = CC_STATE_NEXT;
flag_off:
#if ENABLED_USART
			usart_buffer_clear();
#endif
			EIFR |= (1<<INTF0);
		default:
			break;
		}
	}
}

static void cc_demo_framesec_next(void)
{
	static unsigned char current_frame_sec;
	if (++current_frame_sec == 60) {
		current_frame_sec = 0;
		switch (cc_demo_state) {
		case CC_STATE_WAIT:
			cc_demo_state = CC_STATE_NEXT;
			PORTB |= (1<<LED_BUILTIN_PIN);
			break;
		default:
			break;
		}
	}
}

static void cc_nextframe(void)
{
	static const char *p_cc_f1_demo_data = cc_f1_demo_data;
	static const char *p_cc_f1_error_data;
	unsigned char cc_char[2];

	switch (cc_demo_state) {
	case CC_STATE_END:
#if ENABLED_USART
		if (!usart_buffer_read_bytes(cc_char, 2))
			goto no_data;
		if (cc_char[0] & 0x7f)
			goto add_data;
		if (cc_char[1] & 0x7f) {
			/* Realign Closed Caption data */
			cc_char[0] = cc_char[1];
			cc_char[1] = 0x80;
			usart_buffer_read_bytes(cc_char + 1, 1);
			goto add_data;
		}
		goto null_data;
#else
		goto no_data;
#endif

	case CC_STATE_NEXT:
		cc_char[0] = pgm_read_byte(p_cc_f1_demo_data);
		switch (cc_char[0]) {
		case '\0':
			p_cc_f1_demo_data = cc_f1_demo_data;
end_data:
			cc_demo_state = CC_STATE_END;
#if ENABLED_USART
			usart_buffer_clear();
#endif
no_data:
			cc_current_data[0] = cc_current_data[1] = 0x80;
			PORTB &= ~(1<<LED_BUILTIN_PIN);
			break;
		case '\1':
			p_cc_f1_demo_data++;
			cc_demo_state = CC_STATE_WAIT;
#if ENABLED_USART
null_data:
#endif
			cc_current_data[0] = cc_current_data[1] = 0x80;
			PORTB |= (1<<LED_BUILTIN_PIN);
			break;
		default:
			cc_char[1] =
				pgm_read_byte(p_cc_f1_demo_data + 1);
			p_cc_f1_demo_data += 2;
add_data:
			cc_current_data[0] =
#if NTSC_PROGRESSIVE
				cea608_add_parity(cc_char[0]);
#else
				cea608_add_parity(
					cea608_field1(cc_char));
#endif
			cc_current_data[1] =
				cea608_add_parity(cc_char[1]);
			PORTB ^= (1<<LED_BUILTIN_PIN);
			break;
		}
		break;

	case CC_STATE_WAIT:
		break;

	case CC_STATE_FE:
		p_cc_f1_error_data = cc_f1_fe_data;
		cc_demo_state = CC_STATE_FE_NEXT;
		goto error_next_chars;
	case CC_STATE_DOR:
		p_cc_f1_error_data = cc_f1_dor_data;
		cc_demo_state = CC_STATE_DOR_NEXT;
		goto error_next_chars;

	case CC_STATE_FE_NEXT:
	case CC_STATE_DOR_NEXT:
error_next_chars:
		cc_char[0] = pgm_read_byte(p_cc_f1_error_data);
		if (cc_char[0] == '\0')
			goto end_data;
		else {
			cc_char[1] =
				pgm_read_byte(p_cc_f1_error_data + 1);
			p_cc_f1_error_data += 2;
			goto add_data;
		}
		break;
	}
}

#if ENABLED_VIDEO_TEST
static void draw_dashes(unsigned char v)
{
	unsigned char c = 8;
	do {
		while (!(TIFR2 & (1<<OCF2A)))
			;
		if (v & 1)
			PORTB |= (1<<VIDEO_PIXEL_PIN);
		else {
			PORTB &= ~(1<<VIDEO_PIXEL_PIN);
			_NOP();
		}
		TIFR2 |= (1<<OCF2A);
		v >>= 1;
	} while (--c);
	while (!(TIFR2 & (1<<OCF2A)))
		;
	PORTB &= ~(1<<VIDEO_PIXEL_PIN);
	TIFR2 |= (1<<OCF2A);
}

static void draw_dots(unsigned char n)
{
	do {
		PORTB |= (1<<VIDEO_PIXEL_PIN);
		_NOP();
		PORTB &= ~(1<<VIDEO_PIXEL_PIN);
	} while (--n);
}

static void draw_ntsc_video_test(void)
{
	while (!(TIFR1 & (1<<OCF1B)))
		;
	TCNT2 = 0;
#if NTSC_PROGRESSIVE
	if (!(ntsc_scanarea & (1<<NTSC_EVEN_FIELD))) {
#else
	if (
		ntsc_scanline >= NTSC_VIDEO_START_LINE &&
		ntsc_scanline <= NTSC_FIELD_LINES
	) {
		_NOP();
		_NOP();
		_NOP();
#endif
		_NOP();
		TIFR2 |= (1<<OCF2A);
#if NTSC_PROGRESSIVE
		draw_dashes(ntsc_scanline + NTSC_VBI_LINES);
#else
		draw_dashes(ntsc_scanline);
#endif
		draw_dashes(0);
		draw_dots(24);
		TIFR2 |= (1<<OCF2A);
#if NTSC_PROGRESSIVE
		draw_dashes(ntsc_scanline);
#else
		draw_dashes(ntsc_scanline - (NTSC_VIDEO_START_LINE - 1));
#endif
		draw_dashes(0);
	}
#if NTSC_PROGRESSIVE
	else {
#else
	else if (
		ntsc_scanline >=
			NTSC_FIELD_LINES + 1 + NTSC_VIDEO_START_LINE &&
		ntsc_scanline <= NTSC_FRAME_LINES
	) {
#endif
		TIFR2 |= (1<<OCF2A);
		draw_dashes(0);
#if NTSC_PROGRESSIVE
		draw_dashes(ntsc_scanline + NTSC_VBI_LINES);
#else
		draw_dashes(ntsc_scanline);
#endif
		draw_dots(24);
		TIFR2 |= (1<<OCF2A);
		draw_dashes(0);
#if NTSC_PROGRESSIVE
		draw_dashes(ntsc_scanline);
#else
		draw_dashes(ntsc_scanline -
			(NTSC_FIELD_LINES + NTSC_VIDEO_START_LINE));
#endif
	}
}
#endif

#if ENABLED_USART
static void recv_usart_or_error(void)
{
	if (cc_demo_state != CC_STATE_END)
		return;

	switch (usart_buffer_recv_byte_ifavail()) {
	case OK:
	case NO_DATA:
		break;
	case FRAME_ERROR:
		if (cc_demo_state != CC_STATE_FE_NEXT)
			cc_demo_state = CC_STATE_FE;
		goto display_error;
	case DATA_OVERRUN:
		if (cc_demo_state != CC_STATE_DOR_NEXT)
			cc_demo_state = CC_STATE_DOR;
display_error:
		PORTB |= (1<<LED_BUILTIN_PIN);
		break;
	}
}
#endif

static void io_init(void)
{
	power_all_disable();

	DDRB = (1<<LED_BUILTIN_PIN);

	PORTB = ~((1<<LED_BUILTIN_PIN)
		| (1<<VIDEO_PIXEL_PIN) | (1<<VIDEO_SYNC_PIN));
	PORTC = 0xFF;
	PORTD |= ~((1<<PD0) | (1<<PD1)); /* RX & TX */

#if defined(DIDR0) && defined(DIDR1)
	DIDR0 = (1<<ADC5D) | (1<<ADC4D) | (1<<ADC3D)
		| (1<<ADC2D) | (1<<ADC1D) | (1<<ADC0D);
	DIDR1 = (1<<AIN1D) | (1<<AIN0D);
#endif

	line21_init();
	ntsc_init();
#if ENABLED_USART
	usart_buffer_init();
#endif

#if ENABLED_VIDEO_TEST
	if (PIND & (1<<PD2))
		video_test_disable();
	else
		video_test_enable();
#endif

	/* Botão "demo" */
	EICRA = (3<<ISC00); /* Rising edge of INT0 causes interrupts. */
	EIFR |= (1<<INTF0);
}

#if ENABLED_WATCHDOG
static void watchdog_init(void)
{
	cli();
	wdt_reset();
	WDTCSR |= (1<<WDCE) | (1<<WDE);
	WDTCSR = (1<<WDE) | (1<<WDP1); /* 8K cycles ~ 64 ms */
	sei();
}
#endif

int main(void)
{
#if ENABLED_WATCHDOG
	watchdog_init();
#endif
	io_init();
	sei();
	sleep_enable();
	while (1) {
#if ENABLED_WATCHDOG
		wdt_reset();
#endif
#if NTSC_PROGRESSIVE
		if (ntsc_scanarea & (1<<NTSC_ACTIVE_VIDEO)) {
			if (ntsc_scanline == 8)
				line21_draw_line();
#if ENABLED_VIDEO_TEST
			else if (video_test_is_enabled())
				draw_ntsc_video_test();
#endif
		} else {
			switch (ntsc_scanline) {
			case 1:
				if (!(ntsc_scanarea & (1<<NTSC_EVEN_FIELD))) {
					cc_demo_button();
					cc_demo_framesec_next();
					cc_nextframe();
				}
				break;
			case NTSC_LINE21_LINE:
				line21_draw_line();
				break;
			default:
#if ENABLED_USART
				if (ntsc_scanline <= NTSC_VBI_LINES / 2)
					recv_usart_or_error();
#endif
				break;
			}
		}
#else /* !NTSC_PROGRESSIVE */
		switch (ntsc_scanline) {
		case 1:
			cc_demo_button();
			cc_demo_framesec_next();
			cc_nextframe();
			break;
		case NTSC_LINE21_LINE:
		case NTSC_LINE21_LINE + 8:
			line21_draw_line();
			break;
		default:
#if ENABLED_USART
			if (
				ntsc_scanline < 10 || (
				ntsc_scanline > NTSC_FIELD_LINES &&
				ntsc_scanline < NTSC_FIELD_LINES + 10)
			)
				recv_usart_or_error();
#if ENABLED_VIDEO_TEST
			else
#endif
#else
			_NOP();
#endif
#if ENABLED_VIDEO_TEST
			if (video_test_is_enabled())
				draw_ntsc_video_test();
#endif
			break;
		}
#endif /* NTSC_PROGRESSIVE */
		sleep_cpu();
	}
	return 0;
}
