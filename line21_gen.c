#include <avr/cpufunc.h>
#include <avr/io.h>
#include <avr/power.h>

#include "video_pins.h"
#include "ntsc.h"
#include "line21_gen.h"

#if 0
/* Begin of defines for ATmega8 */
#ifndef TIFR1
#define TIFR1 TIFR
#define TIFR2 TIFR
#endif

#ifndef OCR2A
#define OCR2A OCR2
#endif

#ifndef OCF2A
#define OCF2A OCF2
#endif
/* End of defines for ATmega8 */
#endif

unsigned char cc_current_data[] = { 0x80, 0x80 };

static void line21_writebyte(unsigned char c)
{
	unsigned char counter = 8;
	do {
		while (!(TIFR2 & (1<<OCF2A)))
			;
		if (c & 1)
			PORTB |= (1<<VIDEO_PIXEL_PIN);
		else {
			PORTB &= ~(1<<VIDEO_PIXEL_PIN);
			_NOP();
		}
		TIFR2 |= (1<<OCF2A);
		c >>= 1;
		while (!(TIFR2 & (1<<OCF2A)))
			;
		TIFR2 |= (1<<OCF2A);
	} while (--counter);
}

void line21_draw_line(void)
{
	unsigned char cnt;
	TCNT2 = 0;

	while (!(TIFR1 & (1<<OCF1B)))
		;
	TIFR2 |= (1<<OCF2A);

	/* Clock Run-In */
	cnt = 7;
	do {
		while (!(TIFR2 & (1<<OCF2A)))
			;
		PORTB |= (1<<VIDEO_PIXEL_PIN);
		TIFR2 |= (1<<OCF2A);

		while (!(TIFR2 & (1<<OCF2A)))
			;
		PORTB &= ~(1<<VIDEO_PIXEL_PIN);
		TIFR2 |= (1<<OCF2A);
	} while (--cnt);

	/* Start bits */
	cnt = 3;
	do {
		while (!(TIFR2 & (1<<OCF2A)))
			;
		TIFR2 |= (1<<OCF2A);
	} while (--cnt);

	while (!(TIFR2 & (1<<OCF2A)))
		;
	PORTB |= (1<<VIDEO_PIXEL_PIN);
	TIFR2 |= (1<<OCF2A);
	while (!(TIFR2 & (1<<OCF2A)))
		;
	TIFR2 |= (1<<OCF2A);

	/* 2 characters data, 7-bits */
	line21_writebyte(cc_current_data[0]);
	line21_writebyte(cc_current_data[1]);

	/* End of transmission */
	while (!(TIFR2 & (1<<OCF2A)))
		;
	PORTB &= ~(1<<VIDEO_PIXEL_PIN);
}

void line21_init(void)
{
	power_timer2_enable();

	/* t = 2 * (OCR2A + 1) / 16e6 */
	//OCR2A = NTSC_CYCLES_SCANLINE / 32 - 1;
	OCR2A = NTSC_CYCLES_SCANLINE / (32 * 2) - .5;

#ifdef TCCR2B
	TCCR2A = (1<<WGM21) | (0<<WGM20);  /* CTC, TOP=OCR2A */
	TCCR2B = (1<<CS20);                /* no prescaling */
#else
	TCCR2 = (1<<WGM21) | (0<<WGM20)  /* CTC, TOP=OCR2 */
		| (1 << CS20);           /* no prescaling */
#endif
}
