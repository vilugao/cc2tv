#ifndef CEA608_H
#define CEA608_H

#define CC1_RCL "\x14\x20"
#define CC1_BS  "\x14\x21"
#define CC1_DER "\x14\x24"
#define CC1_RU2 "\x14\x25"
#define CC1_RU3 "\x14\x26"
#define CC1_RU4 "\x14\x27"
#define CC1_FON "\x14\x28"
#define CC1_RDC "\x14\x29"
#define CC1_TR  "\x14\x2A"
#define CC1_RTD "\x14\x2B"
#define CC1_EDM "\x14\x2C"
#define CC1_CR  "\x14\x2D"
#define CC1_ENM "\x14\x2E"
#define CC1_EOC "\x14\x2F"

#define CC1_TO1 "\x17\x21"
#define CC1_TO2 "\x17\x22"
#define CC1_TO3 "\x17\x23"

/* Background and Foreground Attribute Codes */

#define CC1_BWO  "\x10\x20"
#define CC1_BWS  "\x10\x21"
#define CC1_BGO  "\x10\x22"
#define CC1_BGS  "\x10\x23"
#define CC1_BBO  "\x10\x24"
#define CC1_BBS  "\x10\x25"
#define CC1_BCO  "\x10\x26"
#define CC1_BCS  "\x10\x27"
#define CC1_BRO  "\x10\x28"
#define CC1_BRS  "\x10\x29"
#define CC1_BYO  "\x10\x2A"
#define CC1_BYS  "\x10\x2B"
#define CC1_BMO  "\x10\x2C"
#define CC1_BMS  "\x10\x2D"
#define CC1_BAO  "\x10\x2E"
#define CC1_BAS  "\x10\x2F"

#define CC1_BT   "\x17\x2D"
#define CC1_FA   "\x17\x2E"
#define CC1_FAU  "\x17\x2F"

/* Special Assignments */

#define CC1_SCN "\x17\x24" /* Standard Charset, Normal size */
#define CC1_SCD "\x17\x25" /* Standard Charset, Double size */

/* Preamble Address Codes */

#define CC1_01R  "\x11\x48"

#define CC1_0100 "\x11\x50"
#define CC1_0104 "\x11\x52"
#define CC1_0108 "\x11\x54"
#define CC1_0112 "\x11\x56"
#define CC1_0116 "\x11\x58"
#define CC1_0120 "\x11\x5A"
#define CC1_0124 "\x11\x5C"
#define CC1_0128 "\x11\x5E"

#define CC1_0200 "\x11\x70"
#define CC1_0204 "\x11\x72"
#define CC1_0208 "\x11\x74"
#define CC1_0212 "\x11\x76"
#define CC1_0216 "\x11\x78"
#define CC1_0220 "\x11\x7A"
#define CC1_0224 "\x11\x7C"
#define CC1_0228 "\x11\x7E"

#define CC1_1200 "\x13\x50"
#define CC1_1204 "\x13\x52"
#define CC1_1208 "\x13\x54"
#define CC1_1212 "\x13\x56"
#define CC1_1216 "\x13\x58"
#define CC1_1220 "\x13\x5A"
#define CC1_1224 "\x13\x5C"
#define CC1_1228 "\x13\x5E"

#define CC1_1300 "\x13\x70"
#define CC1_1304 "\x13\x72"
#define CC1_1308 "\x13\x74"
#define CC1_1312 "\x13\x76"
#define CC1_1316 "\x13\x78"
#define CC1_1320 "\x13\x7A"
#define CC1_1324 "\x13\x7C"
#define CC1_1328 "\x13\x7E"

#define CC1_1400 "\x14\x50"
#define CC1_1404 "\x14\x52"
#define CC1_1408 "\x14\x54"
#define CC1_1412 "\x14\x56"
#define CC1_1416 "\x14\x58"
#define CC1_1420 "\x14\x5A"
#define CC1_1424 "\x14\x5C"
#define CC1_1428 "\x14\x5E"

#define CC1_1500 "\x14\x70"
#define CC1_1504 "\x14\x72"
#define CC1_1508 "\x14\x74"
#define CC1_1512 "\x14\x76"
#define CC1_1516 "\x14\x78"
#define CC1_1520 "\x14\x7A"
#define CC1_1524 "\x14\x7C"
#define CC1_1528 "\x14\x7E"

/* Mid-Row Codes */

#define CC1_MRCR "\x11\x28"
#define CC1_MRCI "\x11\x2E"

unsigned char cea608_channel1(unsigned char);
unsigned char cea608_channel2(unsigned char);
unsigned char cea608_field1(const unsigned char[2]);
unsigned char cea608_field2(const unsigned char[2]);
unsigned char cea608_add_parity(unsigned char);

#endif
