# CCTV

## Switches

The switches below can be enabled or disabled during compilation via GCC parameter `-D` with value of `1` or `0`.

### `ENABLED_USART`

Enabled by default.
Set `ENABLED_USART` to enable the `USART0` for downloading Closed Caption CEA-608 binary data on serial communication.

### `ENABLED_VIDEO_TEST`

Enabled by default.
Set `ENABLED_VIDEO_TEST` to compile with pattern video test.
To show, the demo button should be activacted (PD2 pin shorted to ground) during start-up.

### `ENABLED_WATCHDOG`

Disabled by default.
Set `ENABLED_WATCHDOG` to activate the Watchdog. This is rarely necessary.

### `ENABLED_XONXOFF`

Disabled by default.
Usable if `ENABLED_USART` is set.
Set `ENABLED_XONXOFF` to use software XON XOFF flow control.

### `NTSC_PROGRESSIVE`

Enabled by default.
Set `NTSC_PROGRESSIVE` to generate fake NTSC 240p60, format used by many old console video games.
This compiles to smaller binary hex.
When set to zero, the approximate NTSC 480i30 format is used.
