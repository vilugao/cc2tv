#ifndef LINE21_GEN_H
#define LINE21_GEN_H

#define NTSC_LINE21_LINE 21

extern unsigned char cc_current_data[2];
void line21_draw_line(void);
void line21_init(void);

#endif
