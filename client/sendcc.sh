#!/usr/bin/env bash
if (( $# < 2 )); then
  echo "Usage: $0 output-device file1 [file2 ...]" >&2
  exit 1
fi

output=$1
shift

for f; do
  if [ ! -r "$f" ]; then
    echo "$f: file not granted to read." >&2
    continue
  fi
  printf 'Sending file %s...\n' "$f" >&2
  sleep 3
  case "${f##*.}" in
    gz) gzip -dc "$f" ;;
    bz2) bzip2 -dc "$f" ;;
    xz) xz -dc "$f" ;;
    *) cat "$f" ;;
  esac | ${SENDCC:-./sendcc} "$output"
done
