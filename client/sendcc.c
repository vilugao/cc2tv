#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef __WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#define INTERVAL_MILLISEC (1e3 / 29.97)

static const char *progname;

static const char *serialoutputpath =
#ifdef __WIN32
	"con";
#else
	"stdout";
#endif

static const char *cea608inputfilepath =
#ifdef __WIN32
	"con";
#else
	"stdin";
#endif

static FILE *serialoutput;
static FILE *cea608input;

#ifndef __WIN32
static const struct timespec SLEEPDURATION =
	{ 0, INTERVAL_MILLISEC * 1e6 };
#endif

static char ccword[2];

static void help(void)
{
	fprintf(stderr,
	        "USAGE: %s <SERIAL OUTPUT> [<INPUT CEA-608 FILE>].\n",
	        progname);
}

static int openfile(FILE **stream, const char *path, const char *mode)
{
	*stream = fopen(path, mode);
	if (*stream == NULL) {
		perror(path);
		return 1;
	}
	return 0;
}

static int parseargs(int argc, const char *argv[])
{
	if (argc != 2 && argc != 3) {
		help();
		exit(EXIT_FAILURE);
	}

	serialoutputpath = argv[1];
	if (openfile(&serialoutput, serialoutputpath, "wb") != 0)
		return 1;

	if (argc == 3) {
		cea608inputfilepath = argv[2];
		if (openfile(&cea608input, cea608inputfilepath, "rb") != 0)
			return 1;
	} else {
		cea608input = stdin;
	}

	return 0;
}

static int readword(void)
{
	size_t transferred = fread(ccword, sizeof *ccword,
		                   sizeof ccword / sizeof *ccword, cea608input);
	if (transferred == 0 && !ferror(cea608input))
		return 0;
	if (transferred != 2) {
		if (ferror(cea608input))
			perror(cea608inputfilepath);
		else
			fprintf(stderr, "%s: %s\n", cea608inputfilepath,
			        "Last double byte (word) not found");
		return -1;
	}
	return 1;
}

static int sendword(void)
{
	if ((ccword[0] & 0x7f) == 0 && (ccword[1] & 0x7f) == 0)
		return 0;

	size_t transferred = fwrite(ccword, sizeof *ccword,
		                    sizeof ccword / sizeof *ccword,
		                    serialoutput);
	if (transferred != 2) {
		if (ferror(serialoutput))
			perror(serialoutputpath);
		return -1;
	}

	if (fflush(serialoutput) != 0) {
		perror(serialoutputpath);
		return -1;
	}

	return 1;
}

static int closefiles(void)
{
	if (cea608input != NULL && cea608input != stdin)
		if (fclose(cea608input) != 0) {
			perror(cea608inputfilepath);
			return 1;
		}
	if (serialoutput != NULL && serialoutput != stdout)
		if (fclose(serialoutput) != 0) {
			perror(serialoutputpath);
			return 1;
		}
	return 0;
}

int main(int argc, const char *argv[])
{
	progname = argv[0];

	if (parseargs(argc, argv) != 0)
		goto fail;

	while (1) {
		switch (readword()) {
		case 0:
			goto end;
		case 1:
			break;
		default:
			goto fail;
		}

#ifdef __WIN32
		Sleep(INTERVAL_MILLISEC);
#else
		nanosleep(&SLEEPDURATION, NULL);
#endif

		if (sendword() < 0)
			goto fail;
	}

end:
	if (closefiles() != 0)
		exit(EXIT_FAILURE);
	exit(EXIT_SUCCESS);

fail:
	closefiles();
	exit(EXIT_FAILURE);
}
